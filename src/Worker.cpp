/*! \file Worker.cpp contains the dqmf::Worker class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqmf/Worker.h>
#include <dqmf/InputContext.h>
#include <TObject.h>

namespace 
{
    class InputHandler
    {
      public:
        InputHandler(	dqm_core::InputListener * listener, 
        		const char * name, 
                        int tag, 
                        boost::shared_ptr<TObject> & object )
          : listener_( listener ),
            name_( name ),
            tag_( tag ),
            object_( object )
        { ; }
        
        void operator()()
        {
            dqmf::InputContext::setData( tag_, object_.get() );
            listener_ -> handleInput( name_, *object_.get() );
        }
        
      private:
        dqm_core::InputListener * listener_;
        std::string name_;
        int tag_;
        boost::shared_ptr<TObject> object_;
    };
}

dqmf::Worker::Worker( size_t thread_num )
  : IPCThreadPool( thread_num, &InputContext::createThreadSpecificStorage ),
    closed_( true )
{ ; }

void
dqmf::Worker::close()
{
    boost::mutex::scoped_lock lock( mutex_ );
    waitForCompletion();
    closed_ = true;
}

void 
dqmf::Worker::open()
{
    boost::mutex::scoped_lock lock( mutex_ );
    closed_ = false;
}

void
dqmf::Worker::execute(	dqm_core::InputListener * listener, 
			const char * name, 
                        int tag, 
                        boost::shared_ptr<TObject> & object )
{
    if ( !closed_ )
    {
	addJob( InputHandler( listener, name, tag, object ) );
    }
}
