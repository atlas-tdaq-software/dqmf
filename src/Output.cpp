/*! \file Output.cpp Implements the dqmf::Output class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/Parameter.h>
#include <dqm_core/Result.h>
#include <dqm_core/OutputFactoryT.h>

#include <dqmf/Output.h>
#include <dqmf/InputContext.h>

#include <oh/OHUtil.h>
#include <oh/OHRootReceiver.h>

#include <TObjArray.h>

namespace
{
    dqm_core::OutputFactoryT<dqmf::Output> __factory__( "DefaultOutput" );

    const size_t DefaultThreadsNumber = 5;
    const size_t DefaultPublicationThreshold = 1;
    
    size_t read_number( const std::string & value )
    {
    	std::istringstream in( value );
        size_t number = DefaultThreadsNumber;
        in >> number;
        return number;
    }
}

using namespace boost::posix_time;    

namespace
{
    dqm_core::Result *
    convert_result_from_is( const IPCPartition & partition, const dqmf::is::Result & is_result )
    {
        ptime time( boost::posix_time::from_time_t(is_result.time().c_time())+
                boost::posix_time::microseconds(is_result.time().mksec()) );
                        
	dqm_core::Result * result = new dqm_core::Result( (dqm_core::Result::Status)is_result.status, 0, time );
        
        for( size_t i = 0; i < is_result.tags.size(); i++ )
        {
            result->tags_[is_result.tags[i].name] = is_result.tags[i].value;
        }

        if ( !is_result.objects.empty() )
        {
            TObjArray * array = new TObjArray( is_result.objects.size() );
	    array -> SetOwner(kTRUE);

            for ( size_t i = 0; i < is_result.objects.size(); ++i )
            {
		try
		{
		    OHRootObject object = OHRootReceiver::getRootObject( partition, is_result.objects[i] );
		    array -> AddAt( object.object.release(), i );
		}
		catch( daq::oh::Exception & ex )
		{
		    ers::log( ex );
		}
            }
            result->object_.reset( array );
        }
        
        return result;
    }
}

dqmf::Output::Publisher::Publisher( 
    const IPCPartition & partition, 
    const std::string & server_name, 
    const std::string & provider_name,
    size_t threads_number,
    size_t publication_threshold )
  : IPCPipeline( threads_number ),
    ISInfoDictionary( partition ),
    publication_threshold_( boost::posix_time::seconds( publication_threshold ) ),
    server_name_( server_name ),
    provider_( provider_name.empty()? 0 : new OHRootProvider( partition, server_name_, provider_name ) )
{ ; }
    
void 
dqmf::Output::Publisher::publishResult( const std::string & name, const dqm_core::Result & result, int tag ) 
{            
    if (   result.status_ == dqm_core::Result::Red
	|| result.status_ == dqm_core::Result::Yellow
	|| result.object_ )
    	publish_result(name, result.clone(), tag, dqmf::InputContext::getData());
    else
	addJob( boost::bind( &dqmf::Output::Publisher::publish_result, this, name, result.clone(), tag, (TObject*)0 ) );
}

void 
dqmf::Output::Publisher::publish_result( const std::string & name, 
	dqm_core::Result * r, int tag, TObject * data )
{            
    ERS_DEBUG( 3, "DQ Result " << *r << " for the '" << name << "' parameter" );

    boost::shared_ptr<dqm_core::Result> result( r );
    {
	boost::mutex::scoped_lock lock( mutex_ );
        Results::iterator it = results_.find( name );
	if ( it == results_.end() )
	{
	    results_.insert( std::make_pair( name, result ) );
	}
        else
        {
            if ( 	it->second->status_ == result->status_ 
            	   && ( result->universal_time_ - it->second->universal_time_ ) < publication_threshold_ )
            {
		ERS_DEBUG( 3, "DQ Result " << result << " for the '"
                	<< name << "' parameter was not published "
                        "since the same result has already been published "
                        "less then " << publication_threshold_ << " second(s) ago" );
                return ;
            }
            else
            {
                it->second = result;
            }
        }
    }
    
    dqmf::is::Result is_result;
    
    convert_result( *result.get(), name, is_result, tag, data );

    try {
	if ( tag <= 0 )
	    checkin( server_name_ + "." + name, is_result, true );
	else
	    checkin( server_name_ + "." + name, tag, is_result );
	ERS_DEBUG( 3, "DQ result '" << name << "' object was published to IS with the '" << tag << "' tag" );
    }
    catch( daq::is::Exception & ex ) {
	ers::warning( dqm_core::Exception( ERS_HERE, "Can't publish DQM Result to IS", ex ) );
    }    
}

void 
dqmf::Output::Publisher::convert_result(  
			const dqm_core::Result & result,
			const std::string & name,
			dqmf::is::Result & is_result,
                        int tag,
                        TObject * data )
{
    is_result.status = (dqmf::is::Result::Status)result.status_;
    std::map<std::string,double>::const_iterator it = result.tags_.begin();
    for( ; it != result.tags_.end(); ++it )
    {
	dqmf::is::Tag t;
	t.name = it->first;
	t.value = it->second;
	is_result.tags.push_back( t );
    }

    if ( !provider_.get() )
    {
	return;
    }
    ////////////////////////////////////////////////////////////////////////
    // Publish extra information associated with the Result
    // if it is ROOT histogram or Graph
    ////////////////////////////////////////////////////////////////////////
    if ( data )
    {
	// publish input data if it's not null
        publish_aux_object( data, name, is_result, tag );
    }
    
    const TObject * object = result.object_.get();
    if ( object )
    {
	if ( object->InheritsFrom( TObjArray::Class() ) )
	{
	    TObjArrayIter it( static_cast<const TObjArray*>( object ) );
            TObject * o = 0;
            while ( ( o = it.Next() ) != 0 )
	    {
		publish_aux_object( o, name, is_result, tag );
	    }
	}
	else
	{
	    publish_aux_object( object, name, is_result, tag );
	}
    }
}

void 
dqmf::Output::Publisher::publish_aux_object(    
			const TObject * object,
			const std::string & name,
			dqmf::is::Result & is_result,
                        int tag )
{
    if ( !object )
    {
	ERS_LOG( "Auxiliary object associated with the '" << name << "' DQ Result is NULL" );
	return;
    }

    std::string object_name = name + object->GetName();

    try
    {
	if ( object->InheritsFrom( TH1::Class() ) )
	{
	    provider_ -> publish( *static_cast<const TH1*>( object ), object_name, tag );
	}
	else if ( object->InheritsFrom( TGraph::Class() ) )
	{
	    provider_ -> publish( *static_cast<const TGraph*>( object ), object_name, tag );
	}
	else if ( object->InheritsFrom( TGraph2D::Class() ) )
	{
	    provider_ -> publish( *static_cast<const TGraph2D*>( object ), object_name, tag );
	}
	else
	{
	    ERS_LOG( "Auxiliary object associated with the '" << name
		    << "' DQ Result has unexpected type '" << object->ClassName() << "'" );
	    return;
	}

	std::string n = oh::util::create_info_name( provider_ -> getServerName(), provider_ -> getName(), object_name );
	is_result.objects.push_back( n );

	ERS_DEBUG( 3, "'" << object_name << "' ROOT object associated with the DQ result for the '"
		    << name << "' parameter was published with the '" << tag << "' tag" );
    }
    catch( daq::oh::Exception & ex )
    {
	ERS_LOG( "Can't publish the TObject associated with the DQM Result to IS: " << ex );
    }
}

dqmf::Output::Output( const std::vector<std::string> & parameters )
  : ISInfoReceiver( parameters.size() ? IPCPartition( parameters[0] ) : IPCPartition() ),
    blocked_( true ),
    is_server_name_( parameters.size() > 1 ? parameters[1] : "DQM" ),
    publisher_( 
        partition(),
        is_server_name_,
        parameters.size() > 2 ? parameters[2] : std::string(),
    	parameters.size() > 3 ? ::read_number( parameters[3] ) : DefaultThreadsNumber,
    	parameters.size() > 4 ? ::read_number( parameters[4] ) : DefaultPublicationThreshold )
{
}

dqmf::Output::~Output()
{
    deactivate();
}

void
dqmf::Output::callback( ISCallbackInfo * info )
{
    if ( blocked_ )
    {
	ERS_DEBUG( 1, "This output stream is in blocked state now, DQ Output Listeners will not be notified" );
    	return;
    }
    
    try
    {
    	dqmf::is::Result is_result;
        info->value( is_result );
	ERS_DEBUG( 1, "IS callback was fired for the '" << info->name() << "' DQ parameter" );
        std::unique_ptr<dqm_core::Result> result( convert_result_from_is( partition(), is_result ) );
        notifyLocalListener( info->name(), *result.get() );
    }
    catch( daq::is::InfoNotCompatible & ex )
    {
	ERS_LOG( "IS callback was fired for the the '" 
        	<< info->name() << "' information which is not of the DQM Result type" );
    }     
}

void 
dqmf::Output::notifyLocalListener( const std::string & name, const dqm_core::Result & result )
{
    ERS_DEBUG( 2, "DQ Result " << result << " for the '" << name << "' parameter" );

    Listeners::iterator it = listeners_.find( name );
    if ( it != listeners_.end() )
    {
        it->second->handleResult( name, result );
    }
}

void 
dqmf::Output::publishResult( const std::string & name, const dqm_core::Result & result ) 
{    
    ERS_DEBUG( 2, "DQ Result " << result << " for the '" << name << "' parameter" );

    if ( blocked_ )
    {
	ERS_DEBUG( 1, "This output stream is in blocked state now, DQ result will not be published" );
    	return;
    }    
    
    publisher_.publishResult( name, result, dqmf::InputContext::getTag() );
}

void
dqmf::Output::addListener( const std::string & name, dqm_core::OutputListener * listener ) 
{
    try
    {
        std::string is_name = is_server_name_ + "." + name;
    	subscribe( is_name, &dqmf::Output::callback, this );
	listeners_[is_name] = listener;
	ERS_DEBUG( 1, "Output listener has been subscribed to IS for the '" << name << "' parameter" );
    }
    catch( daq::is::Exception & ex )
    {
    	throw dqm_core::Exception( ERS_HERE, "Can't subscribe the DQM Output Listener to IS", ex );
    }
}
        
void
dqmf::Output::deactivate()
{
    blocked_ = true;
    publisher_.waitForCompletion();
}

void
dqmf::Output::activate()
{
    blocked_ = false;
}
