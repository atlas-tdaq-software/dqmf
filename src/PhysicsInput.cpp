/*! \file Input.cpp contains the dqmf::PhysicsInput class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/InputFactoryT.h>

#include <dqmf/PhysicsInput.h>

#include <rc/Ready4PhysicsInfo.h>

namespace
{
    dqm_core::InputFactoryT<dqmf::PhysicsInput> __factory__( "PhysicsInput" );
    char const * const 				ISObjectName( "RunParams.Ready4Physics" ); 
}

dqmf::PhysicsInput::PhysicsInput ( const std::vector<std::string> & parameters )
  : Input( parameters ),
    ISInfoReceiver( parameters.size() ? IPCPartition( parameters[0] ) : IPCPartition() ),
    m_physics( true ),
    m_active( false )
{
    try {
	subscribe( ISObjectName, &dqmf::PhysicsInput::callback, this,
	    { ISInfo::Created, ISInfo::Updated, ISInfo::Subscribed });
    }
    catch ( daq::is::Exception & ex ) {
	ERS_LOG( "Subscribing to " << ISObjectName << " IS Info failed: " << ex );
        m_physics = true;
    }
}

void 
dqmf::PhysicsInput::callback( ISCallbackInfo * cb )
{
    ERS_LOG( "ISObjectName IS object is updated" );
    
    boost::mutex::scoped_lock lock( m_mutex );
    
    if ( cb->reason() == ISInfo::Deleted ) {
    	updateState( true );
    }
    else {
	try {
	    Ready4PhysicsInfo info;
	    cb->value( info );
	    updateState( info.ready4physics );
	}
	catch ( daq::is::Exception & ex ) {
	    ERS_LOG( "Parsing " << ISObjectName << " IS Info failed: " << ex );
	}
    }
}

void 
dqmf::PhysicsInput::updateState( bool ready4physics )
{
    ERS_LOG( "ready4physics = " << ready4physics );
    
    m_physics = ready4physics;

    if ( m_physics ) {
	activate();
    }
    else {
	deactivate();
    }
}

void
dqmf::PhysicsInput::activate()
{
    if ( m_physics && !m_active ) {
    	Input::activate();
        m_active = true;
    }
}

void
dqmf::PhysicsInput::deactivate()
{
    if ( m_active ) {
    	Input::deactivate();
        m_active = false;
    }
}
