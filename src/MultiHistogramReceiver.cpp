/*! \file MultiHistogramReceiver.cpp contains the dqmf::MultiHistogramReceiver class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqmf/MultiHistogramReceiver.h>

#include <TClass.h>

dqmf::MultiHistogramReceiver::MultiHistogramReceiver(
        const IPCPartition & partition, const std::vector<std::string> & names,
        dqm_core::InputListener * listener, dqmf::Worker & worker) :
        HistogramReceiver(partition, names[0], listener, worker),
        names_(names),
        tag_(0) {
    for (size_t i = 1; i < names_.size(); ++i) {
        subscribe(names_[i], &dqmf::HistogramReceiver::callbackReceived, this, {
                ISInfo::Created, ISInfo::Updated });
    }
}

dqmf::MultiHistogramReceiver::~MultiHistogramReceiver() {
}

void dqmf::MultiHistogramReceiver::notifyListeners(const char * name, int tag,
        TObject * object) {
    tag_ = std::max(tag_, tag);

    boost::mutex::scoped_lock lock(mutex_);
    InputObjects::iterator it = objects_.find(name);
    if (it != objects_.end()) {
        delete it->second;
        it->second = object;
    }
    else {
        objects_[name] = object;
    }

    if (objects_.size() == names_.size()) {
        TObjArray * obja = new TObjArray(objects_.size());
        obja->SetOwner();

        InputObjects::iterator it = objects_.begin();
        for (; it != objects_.end(); ++it) {
            obja->Add(it->second);
        }
        objects_.clear();

        HistogramReceiver::notifyListeners(TObjArray::Class()->GetName(), tag_,
                obja);

        tag_ = 0;
    }
}
