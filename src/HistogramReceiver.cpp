/*! \file HistogramReceiver.cpp contains the dqmf::HistogramReceiver class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <TParameter.h>
#include <TMap.h>
#include <TObjString.h>

#include <dqmf/HistogramReceiver.h>
#include <oh/core/DataTypes.h>
#include <is/infodynany.h>

dqmf::HistogramReceiver::HistogramReceiver( const IPCPartition & partition,
					    const std::string & name,
					    dqm_core::InputListener * listener,
                                            dqmf::Worker & worker )
  : ISInfoReceiver(partition),
    worker_( worker )
{
    addListener( listener );

    subscribe( name, &dqmf::HistogramReceiver::callbackReceived, this, { ISInfo::Created, ISInfo::Updated } );
    ERS_DEBUG( 1, "Subscribed to the '" << name << "' object" );
}

void
dqmf::HistogramReceiver::addListener( dqm_core::InputListener * listener )
{
    listeners_.push_back( listener );
}

void 
dqmf::HistogramReceiver::notifyListeners( const char * name, int tag, TObject * object )
{
    boost::shared_ptr<TObject> ptr( object );
    for ( Listeners::iterator it = listeners_.begin();
	it != listeners_.end(); ++it )
    {
	worker_.execute( *it, name, tag, ptr );
    }
}

void
dqmf::HistogramReceiver::receive( OHRootHistogram & histogram )
{
    ERS_DEBUG( 1, "The '" << histogram.histogram->GetName() << "' histogram has been updated" );

    TNamed * tn = histogram.histogram.release();

    notifyListeners( tn->GetName(), histogram.tag, tn );
}

void
dqmf::HistogramReceiver::receive( OHRootGraph & graph )
{
    ERS_DEBUG( 1, "The '" << graph.graph->GetName() << "' graph has been updated" );

    TNamed * tn = graph.graph.release();

    notifyListeners( tn->GetName(), graph.tag, tn );
}

void
dqmf::HistogramReceiver::receive( OHRootGraph2D & graph )
{
    ERS_DEBUG( 1, "The '" << graph.graph->GetName() << "' graph has been updated" );

    TNamed * tn = graph.graph.release();

    notifyListeners( tn->GetName(), graph.tag, tn );
}

void
dqmf::HistogramReceiver::receive( OHRootEfficiency & eff )
{
    ERS_DEBUG( 1, "The '" << eff.efficiency->GetName() << "' efficiency has been updated" );

    TNamed * tn = eff.efficiency.release();

    notifyListeners( tn->GetName(), eff.tag, tn );
}

#define CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE( _TT_ ) \
    if ( cb->type() == _TT_::type() ) { \
        _TT_ info; \
        cb->value( info ); \
	((OHReceiver*)this)->receive_( cb->name(), info ); \
    } else

#define SUPPORTED_IS_TYPES(decl)\
decl(bool,Boolean)		\
decl(char,S8)			\
decl(unsigned char,U8)		\
decl(short,S16)			\
decl(unsigned short,U16)	\
decl(int,S32)			\
decl(unsigned int,U32)		\
decl(int64_t,S64)		\
decl(uint64_t,U64)		\
decl(float,Float)		\
decl(double,Double)

#define CONVERT_ATTRIBUTE(X,Y)	\
	case ISType::Y : \
	{ \
	    if (!any.isAttributeArray(i)) { \
		a = new TParameter<double>( any.getAttributeDescription(i).name().c_str(), any.getAttributeValue<X>(i)); \
	    } \
	    else { \
		continue ; \
	    } \
	} \
	break;

void
dqmf::HistogramReceiver::callbackReceived(const ISCallbackInfo * cb )
{
    if ( cb->type().subTypeOf( oh::Histogram::type() ) )
    {
	FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
	{
	    ERS_LOG( "Impossible happens: the '" << cb->name()
		<< "' IS object can not be converted to a TObject instance" );
	}
    }
    else
    {
	TMap * map = new TMap();
	try {
	    ISInfoDynAny any;
	    cb->value(any);

	    for (size_t i = 0; i < any.getAttributesNumber(); ++i)
	    {
		TParameter<double> * a;
		switch(any.getAttributeType(i))
		{
		    SUPPORTED_IS_TYPES( CONVERT_ATTRIBUTE )
		    default :
			continue;
		}
		map->Add(new TObjString(any.getAttributeDescription(i).name().c_str()), a);
	    }
	    notifyListeners( cb->name(), cb->tag(), map );
	}
	catch (daq::is::Exception & ex) {
	    ers::warning(ex);
	    delete map;
	}
    }
}
