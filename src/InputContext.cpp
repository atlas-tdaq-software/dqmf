/*! \file InputContext.cpp .
 *  \author Serguei Kolos
 *  \version 1.0
 */

#include <dqmf/InputContext.h>

#include <TH1.h>
#include <TProfile2D.h>
#include <TProfile3D.h>

boost::thread_specific_ptr<int> dqmf::InputContext::tag_;
boost::thread_specific_ptr<TObject*> dqmf::InputContext::data_;

void
dqmf::InputContext::createThreadSpecificStorage()
{
    tag_.reset( new int( -1 ) );
    data_.reset( new (TObject*)( 0 ) );
}

void
dqmf::InputContext::setData( int tag, TObject * data )
{
    int * tptr = tag_.get( );
    if ( tptr )
    {
    	*tptr = tag;
    }
    
    TObject ** dptr = data_.get( );
    if ( dptr )
    {
    	*dptr = data;
    }
}

int
dqmf::InputContext::getTag( )
{
    int * tptr = tag_.get( );
    if ( tptr )
    {
    	return *tptr;
    }
    return -1;
}

TObject *
dqmf::InputContext::getData( )
{
    TObject ** dptr = data_.get( );
    if ( dptr )
    {
    	return *dptr;
    }
    return 0;
}
