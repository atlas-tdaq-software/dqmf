/*! \file Input.cpp contains the dqmf::Input class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/InputFactoryT.h>

#include <dqmf/Input.h>
#include <dqmf/HistogramReceiver.h>
#include <dqmf/MultiHistogramReceiver.h>

namespace
{
    dqm_core::InputFactoryT<dqmf::Input> __factory__( "DefaultInput" );
}
    
namespace
{
    const size_t DefaultThreadsNumber = 1;
    
    size_t read_size( const std::string & value )
    {
    	std::istringstream in( value );
        size_t number = DefaultThreadsNumber;
        in >> number;
        return number;
    }
}

dqmf::Input::Input ( const std::vector<std::string> & parameters )
  : partition_( parameters.size() ? IPCPartition( parameters[0] ) : IPCPartition() ),
    worker_( parameters.size() > 1 ? ::read_size( parameters[1] ) : DefaultThreadsNumber )
{ ; }

dqmf::Input::~Input()
{
    worker_.close();
}

void
dqmf::Input::addListener( const std::string & name, 
			  dqm_core::InputListener * listener ) 
{
    Receivers::iterator it = receivers_.find( name );
    if ( it != receivers_.end() )
    {
    	it->second->addListener( listener );
        return;
    }
    
    try {
    	receivers_[name] = boost::shared_ptr<HistogramReceiver>( 
        	new HistogramReceiver( partition_, name, listener, worker_ ) );
    }
    catch( ers::Issue & ex ) {
    	throw dqm_core::Exception( ERS_HERE, ex.what(), ex );
    }
}

void
dqmf::Input::addListener( const boost::regex & ,
			  dqm_core::InputListener * ) 
{
    throw dqm_core::Exception( ERS_HERE,
	"The Online DQM framework does not support regular expressions in the input" );
}

void
dqmf::Input::addListener( const std::vector<std::string> & names, 
			  dqm_core::InputListener * listener ) 
{
    ERS_ASSERT_MSG( names.size(), "Empty vector of histogram names is given as input" );
    
    std::string id;
    for ( size_t i = 0; i < names.size(); ++i )
    {
    	id += names[i];
    }
    
    Receivers::iterator it = receivers_.find( id );
    if ( it != receivers_.end() )
    {
    	it->second->addListener( listener );
        return;
    }
    
    try {
    	receivers_[id] = boost::shared_ptr<HistogramReceiver>( 
        	new MultiHistogramReceiver( partition_, names, listener, worker_ ) );
    }
    catch( ers::Issue & ex ) {
    	throw dqm_core::Exception( ERS_HERE, ex.what(), ex );
    }
}

void
dqmf::Input::deactivate()
{
    worker_.close();
}

void
dqmf::Input::activate()
{
    worker_.open();
}

void
dqmf::Input::stop()
{
    boost::mutex::scoped_lock lock( mutex_ );
    condition_.notify_one( );
}

void
dqmf::Input::wait()
{
    boost::mutex::scoped_lock lock( mutex_ );
    condition_.wait( lock );
}
