#include <dqmf/AttributeConverter.h>

void 
dqmf::AttributeConverter::convert (
		std::string & value,
		const Configuration & ,
		const ConfigObject & ,
		const std::string & )
{
    if ( value.size() < 3 )
    	return;
    
    ERS_DEBUG( 2, "Converting '" << value << "' attribute" );
        
    if (   value.at( 0 ) == '$'
    	&& value.at( 1 ) == '{'
    	&& value.at( value.size() - 1 ) == '}' )
    {
    	std::string name = value.substr( 2, value.size() - 3 );
        const char * env = getenv( name.c_str() );
        if ( env )
        {
             value = env;
        }
    }
}
