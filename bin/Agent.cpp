#include <RunControl/Common/RunControlCommands.h>

#include <dqmf/Agent.h>

ERS_DECLARE_ISSUE( dqmf, UserCommandFailed, "User command '" << command 
			<< "' has failed for the '" << parameter << "' item",
			((std::string)command)
			((std::string)parameter)
                         )

void 
dqmf::Agent::configure(const daq::rc::TransitionCmd & cmd) 
{ 
    ERS_LOG( "command '" << cmd.toString() << "' received" );
    
    m_config.release();
    
    try {
        auto & s = daq::rc::OnlineServices::instance();
        m_config.reset( new Configuration( s.getConfiguration(),
	        s.getPartition(), s.applicationName() ) );
    }
    catch( ers::Issue & ex ) {
	ERS_LOG( ex );
	throw ; // RC framework will take care of this exception
    }  

    ERS_LOG( "done" );
}

void 
dqmf::Agent::prepareForRun(const daq::rc::TransitionCmd & cmd) 
{ 
    ERS_LOG( "command '" << cmd.toString() << "' received" );
    
    m_config -> activate();
    
    ERS_LOG( "done" );
}

void 
dqmf::Agent::stopArchiving(const daq::rc::TransitionCmd & cmd) 
{ 
    ERS_LOG( "command '" << cmd.toString() << "' received" );
    
    m_config -> deactivate();

    ERS_LOG( "done" );
}

void 
dqmf::Agent::unconfigure(const daq::rc::TransitionCmd & cmd) 
{ 
    ERS_LOG( "command '" << cmd.toString() << "' received" );
        
    m_config.reset( 0 );

    ERS_LOG( "done" );
}

void 
dqmf::Agent::user(const daq::rc::UserCmd & cmd)
{
    ERS_LOG( "user command '" << cmd.toString() << "' received" );

    if ( cmd.commandName() != "enable" && cmd.commandName() != "disable" ) {
	ERS_LOG( "Ignoring unknown user command '" << cmd.commandName() << "'" );
	return;
    }

    if ( !m_config.get() ) {
    	ers::warning( BadState( ERS_HERE ) );
        return ;
    }
    
    std::vector<std::string> params = cmd.commandParameters();
    for (size_t i = 0; i < params.size(); ++i ) {
	try {
	    if ( cmd.commandName() == "enable" ) {
		m_config->enableParameter( params[i] );
		ERS_INFO( "DQ Parameter '" << params[i] << "' has been enabled" );
	    }
	    else if ( cmd.commandName() == "disable" ) {
		m_config->disableParameter( params[i] );
		ERS_INFO( "DQ Parameter '" << params[i] << "' has been disabled" );
	    }
	}
	catch ( ers::Issue& ex ) {
	    ers::error( UserCommandFailed( ERS_HERE, cmd.toString(), params[i], ex ) );
	}
    }
}
