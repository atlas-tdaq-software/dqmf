/*! \file Configuration.cpp Implements the dqmf::Configuration class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/bind/bind.hpp>

#include <dqm_core/LibraryManager.h>
#include <dqm_core/IOManager.h>
#include <dqm_core/Region.h>
#include <dqm_core/InputMultiplexor.h>
#include <dqm_core/OutputMultiplexor.h>
#include <dqm_core/Utils.h>

#include <dqmf/AttributeConverter.h>
#include <dqmf/Configuration.h>

#include <config/Change.h>
#include <config/ConfigObject.h>

#include <dal/Segment.h>
#include <dal/Partition.h>
#include <dal/util.h>

#include <ResourcesInfo/utils.h>

#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQInput.h>
#include <dqm_config/dal/DQOutput.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQSummaryMaker.h>

#include <dqm_config/AlgorithmConfig.h>

ERS_DECLARE_ISSUE( dqmf, AgentNotFound, "DQ Agent '" << name << "' is not described in the database", 
			((std::string)name))

ERS_DECLARE_ISSUE( dqmf, NoStreams, "No " << type << " stream for the '" 
					  << agent << "' DQ Agent are available", 
			((std::string)type) ((std::string)agent))

ERS_DECLARE_ISSUE( dqmf, CantCreateStream, "The " << type << " stream '" << name << "' for the '" 
						  << agent << "' DQ Agent can not be created", 
			((std::string)type) ((std::string)name) ((std::string)agent))

ERS_DECLARE_ISSUE( dqmf, PartitionNotFound, "Partition '" << name 
			<< "' is not described in the database", 
			((std::string)name))

ERS_DECLARE_ISSUE( dqmf, ParameterNotFound, "Parameter '" << name 
			<< "' is not found in the DQM configuration", 
			((std::string)name))

dqmf::Configuration::Configuration(
        ::Configuration &config,
        const daq::core::Partition &partition,
        const std::string & agent_name )
  : m_config( config ),
    m_partition( partition ),
    m_algo_factory( m_partition.UID() ),
    m_agent_name( agent_name ),
    m_agent( m_config.get<dqm_config::dal::DQAgent>( m_agent_name ) ),
    m_callback_handler( 0 ),
    m_active( false )
{    
    if ( !m_agent )
    {
	throw dqmf::AgentNotFound( ERS_HERE, m_agent_name );
    }

    m_config.register_converter( new dqmf::AttributeConverter() );
    
    createIOStreams( );

    m_agent -> visit_all_regions(
	    std::bind(&dqmf::Configuration::build_region, this,
	            std::placeholders::_1, Helper()));
        
    try {
    	m_callback_handler = 
        	daq::ResourcesInfo::subscribe(	
                	m_partition.UID(), m_config, m_partition,
			boost::bind( &dqmf::Configuration::checkDisabled, this ) );
    }
    catch( daq::ResourcesInfo::Exception & ex ) {
    	ers::error( ex );
    }

    m_algo_factory.getReferenceManager().publishReferences();
}

dqmf::Configuration::~Configuration()
{    
    deactivate();
        
    try {
    	daq::ResourcesInfo::unsubscribe( m_callback_handler );
    }
    catch( daq::ResourcesInfo::Exception & ex ) {
    	ers::warning( ex );
    }    
}

void 
dqmf::Configuration::activate()
{
    m_algo_factory.updateRunParameters();
    m_output -> activate();
    reset( );
    m_input -> activate();
    m_active = true;
}

void 
dqmf::Configuration::deactivate()
{
    m_input -> deactivate();
    m_output -> deactivate();
    m_active = false;
}

void 
dqmf::Configuration::enableParameter( const std::string& name )
{
    if ( !m_active ) {
    	throw BadState( ERS_HERE );
    }
    
    bool r = dqm_core::enable(name, m_root_regions.begin(), m_root_regions.end(), false );
    if ( !r ) {
    	throw ParameterNotFound( ERS_HERE, name );
    }
}

void 
dqmf::Configuration::disableParameter( const std::string& name )
{
    if ( !m_active ) {
    	throw BadState( ERS_HERE );
    }
    
    bool r = dqm_core::disable(name, m_root_regions.begin(), m_root_regions.end(), false );
    if ( !r ) {
    	throw ParameterNotFound( ERS_HERE, name );
    }
}

void 
dqmf::Configuration::reset()
{
    ERS_LOG( "Reset regions/parameters status to Undefined/Disabled ... " );
    
    for ( size_t i = 0; i < m_root_regions.size(); i++ )
    {
	m_root_regions[i] -> reset( false, true );
    }
    
    ERS_LOG( "All regions/parameters statuses were reset to Undefined/Disabled" );
}

bool 
dqmf::Configuration::isEnabled( const dqm_config::dal::DQNode * node )
{
    const std::vector<const daq::core::Component *> & resources = node -> get_AssociatedResources();

    if ( !resources.size() )
    {
	ERS_DEBUG( 3, "'" << node->UID() << "' Parameter has no associated resources" );
	return true;
    }

    bool is_enabled = true;
    for ( size_t i = 0; i < resources.size(); i++ )
    {
	is_enabled = !resources[i] -> disabled( m_partition );
	ERS_DEBUG( 2, "resource '" << resources[i] -> UID() << "' is " 
        		<< ( is_enabled ? "enabled" : "disabled" ) );

	if ( is_enabled )
	{
	    break;
	}
    }
    
    return is_enabled;
}

bool
dqmf::Configuration::referencedByAgent(const dqm_config::dal::DQRegion * region) const
{
    return region->is_referenced_by_agent();
}


bool
dqmf::Configuration::build_region( const dqm_config::dal::DQRegion * region,
				  dqmf::Configuration::Helper & config_helper )
{
    ERS_DEBUG( 1, "Build up region '" << region->UID() << "' with algorithm "<< region->get_DQSummaryMaker()->UID() );
    
    try
    {
	dqm_core::LibraryManager::instance().loadLibrary( region->get_DQSummaryMaker()->get_LibraryName() );
   	
	bool region_enabled = isEnabled( region );
        
	if ( config_helper.m_branch_disabled )
	{
	    region_enabled = false;
	}
	
        ERS_DEBUG( 1, "Region '" << region->UID() << "' is " << ( region_enabled ? "enabled" : "disabled" ) );
	
        config_helper.m_branch_disabled = !region_enabled;
	
        dqm_core::RegionConfig rconfig( 
        		region->get_DQSummaryMaker()->UID(),
                        region->get_Weight(),
                        region_enabled );
                        
    	if ( !config_helper.m_current_parent.get() ) {
	    config_helper.m_current_parent = dqm_core::Region::createRootRegion( 
            		region->UID(), 
            		*m_input,
                        *m_output,
                        rconfig );
            m_root_regions.push_back( config_helper.m_current_parent );
    	}
    	else {
	    if ( referencedByAgent(region) ) {
		config_helper.m_current_parent -> addRemoteRegion( region->UID(), rconfig );
		return false;
            }
            else {
		RegionPtr new_region = config_helper.m_current_parent -> addRegion( region->UID(), rconfig );		
		if ( new_region->parentsNumber() > 1 )
                {
                    // This region was already processed since it is referenced more than once
                    return false;
                }
                config_helper.m_current_parent = new_region;
	    }
    	}
    }
    catch( ers::Issue & ex )
    {
	ers::error( ex );
	return false;
    }
    
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for ( size_t i = 0; i < parameters.size(); ++i )
    {
	try {
	    ERS_DEBUG( 1, "Parameter UID: " << parameters[i]->UID() 
            		<< " and algo: "<< parameters[i]->get_DQAlgorithm()->UID() );
	    
	    dqm_core::LibraryManager::instance().loadLibrary( parameters[i]->get_DQAlgorithm()->get_LibraryName() );
            
            const std::vector<std::string> & input = parameters[i]->get_InputDataSource();
	    
            bool is_enabled = !config_helper.m_branch_disabled && isEnabled( parameters[i] );
            
	    std::shared_ptr<dqm_config::AlgorithmConfig> algo_config =
		m_algo_factory.createAlgorithmConfig( *parameters[i] );
                                
	    if ( input.empty() )
	    {
		ers::error( dqm_core::BadConfig( ERS_HERE, parameters[i]->UID(), "no input data provided" ) );
	    }
            else
	    {
		dqm_core::ParameterConfig pconfig(  input,				    // input for the algo
						    parameters[i]->get_DQAlgorithm()->UID(),// algo name
						    parameters[i]->get_Weight(),	    // weight i.e. severity level for that parameter
						    algo_config,		    	    // configuration of the algo
                                                    is_enabled );
		config_helper.m_current_parent -> addParameter( parameters[i]->UID(), pconfig );
	    }
	}
	catch( ers::Issue & ex ) {
	    ers::error( ex );
	}
    }
    return true;
}

void
dqmf::Configuration::createIOStreams( )
{
    const std::vector<const dqm_config::dal::DQInput *> & inputs = m_agent->get_DQInputs();
    if ( !inputs.size() )
    {
	throw dqmf::NoStreams( ERS_HERE, "input", m_agent_name );
    }

    const std::vector<const dqm_config::dal::DQOutput *> & outputs = m_agent->get_DQOutputs();
    if ( !outputs.size() )
    {
	throw dqmf::NoStreams( ERS_HERE, "output", m_agent_name );
    }

    if ( inputs.size() == 1)
    {
	dqm_core::LibraryManager::instance().loadLibrary( inputs[0]->get_Library() );
	m_input.reset( dqm_core::IOManager::instance().createInput( 
        			inputs[0]->get_Name(), inputs[0]->get_Parameters() ) );
    }
    else
    {
	dqm_core::InputMultiplexor * min = new dqm_core::InputMultiplexor( );
	size_t streams_created = 0;
	for ( size_t i = 0; i < inputs.size(); i++ )
	{
	    try {
		dqm_core::LibraryManager::instance().loadLibrary( inputs[i]->get_Library() );
		min -> addInput( dqm_core::IOManager::instance().createInput( 
                		inputs[i]->get_Name(), inputs[i]->get_Parameters() ) );
		++streams_created;
            }
            catch( dqm_core::Exception & ex )
            {
            	ers::error( CantCreateStream( ERS_HERE, "input", inputs[i]->UID(), m_agent_name, ex ) );
            }
	}

        if ( !streams_created )
        {
	    throw dqmf::NoStreams( ERS_HERE, "input", m_agent_name );
        }

	m_input.reset( min );
    }

    if ( outputs.size() == 1)
    {
	dqm_core::LibraryManager::instance().loadLibrary( outputs[0]->get_Library() );
	m_output.reset( dqm_core::IOManager::instance().createOutput( 
        			outputs[0]->get_Name(), outputs[0]->get_Parameters() ) );
    }
    else
    {
	dqm_core::OutputMultiplexor * mout = new dqm_core::OutputMultiplexor( );
	size_t streams_created = 0;
        for ( size_t i = 0; i < outputs.size(); i++ )
	{
	    try {
            	dqm_core::LibraryManager::instance().loadLibrary( outputs[i]->get_Library() );
	    	mout -> addOutput( dqm_core::IOManager::instance().createOutput( 
                			outputs[i]->get_Name(), outputs[i]->get_Parameters() ) );
                ++streams_created;
            }
            catch( dqm_core::Exception & ex )
            {
            	ers::error( CantCreateStream( ERS_HERE, "output", outputs[i]->UID(), m_agent_name, ex ) );
            }
	}
	
        if ( !streams_created )
        {
	    throw dqmf::NoStreams( ERS_HERE, "output", m_agent_name );
        }
        
        m_output.reset( mout );
    }
}

bool
dqmf::Configuration::update_status( const dqm_config::dal::DQRegion * region )
{
    ERS_DEBUG( 1, "Checking '" << region->UID() << "' Region status" );
        
    bool changed = maybeChangeNodeState( region, true /* disable children */ );
    
    if ( changed )
    {
	ERS_LOG( "The states of all children of the '" << region->UID()
        		<< "' Region have been changed" );
	return false ;
    }
    
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for ( size_t i = 0; i < parameters.size(); i++ )
    {
	maybeChangeNodeState( parameters[i] );
    }
    return true;
}

bool
dqmf::Configuration::maybeChangeNodeState( const dqm_config::dal::DQNode * node_config, bool disable_children )
{
    ERS_DEBUG( 2, "Checking '" << node_config->UID() << "' Parameter status" );
    
    const std::vector<const daq::core::Component *> & resources = node_config -> get_AssociatedResources();
    
    if ( !resources.size() )
    {
	ERS_DEBUG( 2, "'" << node_config->UID() << "' Parameter has no associated resources" );
        return false;
    }
    
    bool shall_be_enabled = isEnabled( node_config );
    
    boost::shared_ptr<dqm_core::Node> node = dqm_core::find( node_config -> UID(),
						m_root_regions.begin(),
						m_root_regions.end() );
    if ( !node )
    {
	ERS_LOG( "'" << node_config -> UID() << "' Region/Parameter not found in memory " );
        return false;
    }
    
    bool actually_enabled = ( node -> getResult() -> status_ != dqm_core::Result::Disabled );
    
    if ( shall_be_enabled != actually_enabled )
    {
	node -> stateChanged( shall_be_enabled, disable_children );
	ERS_LOG( "'" << node_config -> UID() << "' Region/Parameter is "
        	<< ( shall_be_enabled ? "enabled" : "disabled" ) );
	return true;
    }
    else
    {
	ERS_DEBUG( 2, "'" << node_config -> UID() << "' Region/Parameter retains its "
        	<< ( actually_enabled ? "enabled" : "disabled" ) << " state" );
	return false;
    }
}

void 
dqmf::Configuration::checkDisabled( )
{
    ERS_LOG( "check and disable regions if necessary ... " );
    
    try {
	m_agent -> visit_all_regions(
		std::bind(&dqmf::Configuration::update_status, this, std::placeholders::_1));
    }    
    catch( ers::Issue & ex )
    {
	ers::error( ex );
	return ;
    }
    
    ERS_LOG( "configuration was updated" );
}

