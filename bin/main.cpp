/* Agent.cpp
   Serguei Kolos    30.08.2013
*/

#include <boost/program_options.hpp>

#include <dqmf/Agent.h>
#include <RunControl/Common/CmdLineParser.h>
#include <RunControl/Common/Exceptions.h>
#include <RunControl/ItemCtrl/ItemCtrl.h>

#include <TString.h>
#include <TSystem.h>
#include <TThread.h>

int main(int argc, char **  argv) 
{      
    TThread::Initialize();

    try {
        daq::rc::CmdLineParser cmdParser(argc, argv, true);

        if ( gSystem )
        {
            gSystem->ResetSignal((ESignals)6,true);
            gSystem->ResetSignal((ESignals)11,true);
        }

        daq::rc::ItemCtrl ctrl(cmdParser, std::make_shared<dqmf::Agent>());

        ctrl.init();

        ERS_LOG( "dqmf_agent has been started in the '"
                << cmdParser.partitionName() << "' partition." );

        ctrl.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << "Implements Data Quality Monitoring framework." << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    ERS_LOG( "dqmf_agent has been stopped " );
            
    return EXIT_SUCCESS;
}

