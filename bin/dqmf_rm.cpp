/* dqmf_rm.cpp
   Author Serguei Kolos 22.07.2009
*/

#include <vector>

#include <boost/program_options.hpp>

#include <ipc/partition.h>

#include <is/infodictionary.h>

#include <dqm_config/is/DQParameter.h>

#include <RunControl/Common/Controllable.h>
#include <RunControl/Common/CmdLineParser.h>
#include <RunControl/Common/Exceptions.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/ItemCtrl/ItemCtrl.h>

namespace dqmf 
{
    class InfoCleaner : public daq::rc::Controllable
    {
      public:

	InfoCleaner(	const IPCPartition & partition,
			const std::vector<std::string> & servers )
	  : m_dictionary( partition ),
	    m_servers( servers )
	{ ; }
        
	~InfoCleaner() noexcept {
	    cleanup();
	}
        
      private:
        void unconfigure(const daq::rc::TransitionCmd & cmd)
        {
            ERS_LOG( "command '" << cmd.toString() << "' received" );

            cleanup();
        }

        void cleanup()
        {
            for ( size_t i = 0; i < m_servers.size(); ++i ) {
                try {
                    m_dictionary.removeAll( m_servers[i], dqm_config::is::DQParameter::type() );
                }
                catch( daq::is::Exception & ex ) {
                    ERS_LOG( ex );
                    continue;
                }
                ERS_LOG( "All DQ Parameters have been removed from the '"
                    << m_servers[i] << "' IS server" );
            }
        }

      private:
	ISInfoDictionary		m_dictionary;
	std::vector<std::string>	m_servers;
    };
}

int main(int argc, char **  argv) 
{
    namespace po = boost::program_options;
    
    po::options_description desc(
    	"Removes all objects of the dqm_config::is::DQParameter type from the given IS servers.");
        
    try {
        std::vector<std::string> servers;
        desc.add_options()("list,l", 
        	po::value<std::vector<std::string>>(&servers)->multitoken(), "names of IS servers");
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        po::notify(vm);
        
        daq::rc::CmdLineParser cmdParser(argc, argv, true);

	IPCPartition partition( cmdParser.partitionName() );

	std::shared_ptr<daq::rc::Controllable> cleaner(
        	new dqmf::InfoCleaner(partition, servers));

	daq::rc::ItemCtrl ctrl(cmdParser, cleaner);

	ctrl.init();

	ERS_LOG( "dqmf_rm has been started in the '"
	    	<< partition.name() << "' partition." );

	ctrl.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << desc << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    ERS_LOG( "dqmf_rm has been stopped." );
            
    return EXIT_SUCCESS;
}

