#ifndef _DQMF_ATTRIBUTE_CONVERTER_H_
#define _DQMF_ATTRIBUTE_CONVERTER_H_

/*! \file AttributeConverter.h Declares the dqmf::AttributeConverter class.
 * \author Serguei Kolos
 * \version 1.0
 */


#include <config/Configuration.h>

namespace dqmf
{
    struct AttributeConverter : public Configuration::AttributeConverter<std::string>
    {        
        void convert (	std::string & value, 
        		const Configuration & conf,
                        const ConfigObject & obj,
                        const std::string & attr_name );
    };
}

#endif
