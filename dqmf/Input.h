#ifndef _DQMF_INPUT_H_
#define _DQMF_INPUT_H_

/*! \file Input.h Declares the dqmf::Input class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <unordered_map>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <dqm_core/Input.h>

#include <ipc/partition.h>

#include <dqmf/HistogramReceiver.h>
#include <dqmf/Worker.h>

class OHRootReceiver;

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Framework.
 */
namespace dqmf
{
    /*! \brief This class provides a simple basic implementation of the DQMF abstract input interface
    		which can be used for testing the core of DQMF.
	\ingroup public
	\sa	dqm_core::Input
	\sa	dqm_core::InputListener
     */
    struct Input : public dqm_core::Input,
    		   boost::noncopyable
    {        
        /*! Constructor.
	 */
        Input ( const std::vector<std::string> & parameters );
        
	/*! Destructor.
	 */
        ~Input ();
        
	/*! This function is called to register a listener object with a particular object in the
        	input stream. The InputListener::handleInput function will be called by this Input
                stream whenever an object with the given name becomes available in the stream.
	    \param name objects's name
	    \param listener listener associated with the given object name
	 */
        void addListener( const std::string & name, dqm_core::InputListener * listener ) ;
        
	/*! This function is called to register a listener object with a set of objects in the
        	input stream. The InputListener::handleInput function will be called by this Input
                stream whenever a single object from this set becomes available in the stream.
	    \param regex Posix style regular expression for the objects' names
	    \param listener listener associated with the given regular expression
	 */
        void addListener( const boost::regex & regex, dqm_core::InputListener * listener ) ;

	/*! This function is called to register a listener object with a set of objects in the
        	input stream. The InputListener::handleInput function will be called by this Input
                stream whenever a single object from this set becomes available in the stream.
	    \param names vecotor of objects' names
	    \param listener listener associated with the given objects' set
	 */
        void addListener( const std::vector<std::string> & names, dqm_core::InputListener * listener ) ;
        
	/*! This function blocks this Input stream until the Input::activate function is called. Blocking
        	means that when this function returns it is guaranteed that listeners which have been
                registered with this stream are notified anymore. The function has no effect if the
                stream has been already in the blocked state.
            \sa activate
	 */
        void deactivate();
        
	/*! This function activates this Input stream until the Input::block function is called. Activating
        	means that the stream will start notifying the listeners which have been registered with it.
            \sa deactivate
	 */
        void activate();
        
        /*! This function causes termination of the wait function.
            \sa block
	 */
        void stop();
                
        /*! This function blocks until there is some data still in this Input stream.
            \sa block
	 */
        void wait();
                
      private:
        typedef std::unordered_map< std::string, boost::shared_ptr<HistogramReceiver> > Receivers;

	IPCPartition		partition_;
        Worker			worker_;
      	boost::mutex		mutex_;
      	boost::condition	condition_;
        Receivers		receivers_;
    };
}

#endif
