#ifndef _DQMF_WORKER_H_
#define _DQMF_WORKER_H_

#include <iostream>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>

#include <ipc/threadpool.h>

#include <dqm_core/InputListener.h>

namespace dqmf
{
    class Worker : public IPCThreadPool
    {      
      public:
        Worker( size_t thread_num = 1 );
        
        void close();
    	void open();
        
        void execute(	dqm_core::InputListener * listener, 
        		const char * name,
                        int tag, 
                        boost::shared_ptr<TObject> & object );
        
      private:
      	boost::mutex	mutex_;
        bool		closed_;
    };
}

#endif
