#ifndef _DQMF_MULTI_HISTOGRAM_RECEIVER_H_
#define _DQMF_MULTI_HISTOGRAM_RECEIVER_H_

/*! \file HistogramReceiver.h Declares the dqmf::MultiHistogramReceiver class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <vector>

#include <TObjArray.h>

#include <dqmf/HistogramReceiver.h>

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Framework.
 */
namespace dqmf
{
    /*! \brief This class implements the OH Root histogram receiver interface.
	\ingroup public
	\sa	dqmf::Input
     */
    class MultiHistogramReceiver : public HistogramReceiver
    {
      public:
	
        MultiHistogramReceiver(	const IPCPartition & partition, 
        			const std::vector<std::string> & names,
                                dqm_core::InputListener * listener,
                                dqmf::Worker & worker );
    
        /*! Destructor.
	 */
        ~MultiHistogramReceiver ();
        
      private:
                
        void notifyListeners( const char * name, int tag, TObject * object );
        
        typedef std::map<std::string, TObject *> InputObjects;
        
      	boost::mutex			mutex_;
        std::vector<std::string>	names_;
        InputObjects			objects_;
        int				tag_;
    };
}

#endif
