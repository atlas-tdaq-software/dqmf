/* \author Serguei Kolos
 * \version 1.0
 */

#include <memory>

#include <RunControl/Common/Controllable.h>
#include "RunControl/Common/OnlineServices.h"
#include <dqmf/Configuration.h>

namespace dqmf
{         
    class Agent : public daq::rc::Controllable
    {
      public:
    
	void configure(const daq::rc::TransitionCmd&);
                
	void prepareForRun(const daq::rc::TransitionCmd&);
                
	void stopArchiving(const daq::rc::TransitionCmd&);
        
	void unconfigure(const daq::rc::TransitionCmd&);
        
        void user(const daq::rc::UserCmd&);

      private:
        std::unique_ptr<Configuration> m_config;
    };
}
