#ifndef _DQMF_OUTPUT_H_
#define _DQMF_OUTPUT_H_

/*! \file Output.h Declares the dqmf::Output class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <unordered_map>
#include <boost/thread.hpp>

#include <dqm_core/Output.h>
#include <dqm_core/OutputListener.h>
#include <dqmf/is/Result.h>

#include <ipc/threadpool.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <oh/OHRootProvider.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqmf
{
    /*! \brief This class provides a simple implementation of the abstract output interface for publishing infomation
    		which is produced by the Data Quality assessment algorithms.
	\ingroup public
        \sa dqm_core::Output
     */
    class Output : public dqm_core::Output,
    		   public ISInfoReceiver
    {
	class Publisher : public IPCThreadPool,
        		  protected ISInfoDictionary
	{
	  public:
            Publisher(	const IPCPartition & partition, 
			const std::string & server_name,
			const std::string & provider_name,
			size_t threads_number,
                        size_t publication_threshold );
	    
            void publishResult( const std::string & name, const dqm_core::Result & result, int tag );

	  private:
          
	    void publish_result( const std::string & name, dqm_core::Result * result, 
            			 int tag, TObject * input_data = 0 );
            
	    void publish_aux_object(	const TObject * object,
					const std::string & name,
					dqmf::is::Result & is_result,
                                        int tag );

	    void convert_result(	const dqm_core::Result & result,
					const std::string & name,
					dqmf::is::Result & is_result,
                                        int tag, TObject * input_data );
	  private:
	    typedef std::unordered_map< std::string, boost::shared_ptr<dqm_core::Result> > Results;
	    
            boost::posix_time::time_duration publication_threshold_;
            std::string server_name_;
	    boost::shared_ptr<OHRootProvider> provider_;
	    boost::mutex mutex_;
	    Results results_;
	};

      public:
      	Output( const std::vector<std::string> & parameters );
        
        ~Output();
        
        /*! This function is used to put the given DQ result to the appropriate place.
	 */
	virtual void publishResult( const std::string & name, const dqm_core::Result & result );
        
	/*! This function is called to register a listener object with a particular object in this
        	output stream. The OutputListener::handleResult function will be called by this Output
                stream whenever a dqm_core::Result object for the dqm_core::Parameter with the given 
                name becomes available in this stream.
	    \param name parameters' name
	    \param listener listener associated with the given parameter name
	 */
	virtual void addListener( const std::string & name, dqm_core::OutputListener * listener );
                
	/*! This function activates this Output stream until the Output::deactivate function is called. 
        	Activating means that the stream will accept the DQ result and pass them to the appropriate
                place and will also start notifying the listeners which have been registered with it.
            \sa deactivate
	 */
        virtual void activate();
	
	/*! This function blocks this Output stream until the Output::activate function is called. Blocking
        	means that when this function returns it is guaranteed that listeners which have been
                registered with this stream will not be notified anymore and any calls to the Output::publishResult
                will have no effect. The function has no effect if the stream is already in the inactive state.
            \sa activate
	 */
        virtual void deactivate();
        
      private:
        void callback( ISCallbackInfo * info );
        
        void notifyLocalListener( const std::string & name, const dqm_core::Result & result );
        
        typedef std::map<std::string,dqm_core::OutputListener*> Listeners;
        
        bool blocked_;
        const std::string is_server_name_;
        Listeners listeners_;
        Publisher publisher_;
    };
}

#endif
