#ifndef _DQMF_COOL_OUTPUT_H_
#define _DQMF_COOL_OUTPUT_H_

/*! \file CoolOutput.h Declares the dqmf::CoolOutput class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <iostream>
#include <map>

#include <boost/thread.hpp>

#include <dqm_core/Output.h>
#include <dqm_core/OutputListener.h>

#include <is/inforeceiver.h>

#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqmf 
{
    /*! \brief This class provides a simple implementation of the abstract output interface for publishing infomation
    		which is produced by the Data Quality assessment algorithms to the COOL DB.
	\ingroup public
        \sa dqm_core::Output
     */
    class CoolOutput :	public dqm_core::Output
    {
	struct Result
	{
	    Result( cool::ChannelId channel );
            
            dqm_core::Result::Status	status_;
            const cool::ChannelId	channel_;
            cool::UInt63		since_;
            bool			recorded_;
	};
      
      public:
        
        CoolOutput( const std::vector<std::string> & parameters ) ;
        
        ~CoolOutput();
        
      private:
        /*! This function is used to put the given DQ result to an appropriate place.
	 */
        void publishResult( const std::string & name, const dqm_core::Result & result ) ;
        
        /*! This function does nothing for this stream.
	 */
        void addListener( const std::string & , dqm_core::OutputListener * )
        			 { ; }
        
        /*! This function does nothing for this stream.
	 */
        void addListener( const dqm_core::Parameter & , dqm_core::OutputListener * ) { ; }
        
	/*! This function activates this Output stream until the Output::deactivate function is called. 
        	Activating means that the stream will accept the DQ result and pass them to the appropriate
                place and will aslo start notifying the listeners which have been registered with it.
            \sa deactivate
	 */
        void activate();
	
	/*! This function blocks this Output stream until the Output::activate function is called. Blocking
        	means that when this function returns it is guaranteed that listeners which have been
                registered with this stream will not be notified anymore and any calls to the Output::publishResult
                will have no effect. The function has no effect if the stream is already in the inactive state.
            \sa activate
	 */
        void deactivate();
        
	void writeResult( const std::string & channelname, 
			  cool::ChannelId channel,
                          dqm_core::Result::Status status,
                          cool::UInt63 since,
                          cool::UInt63 till ) const;
                          
      private:
        void initialize();
        
        void finalize();
    	
      private:
        std::string			partition_;
        std::string			connect_string_;
        bool				blocked_;
      	boost::mutex			mutex_;
        cool::IDatabasePtr		database_;
        cool::IFolderPtr 		folder_;
        std::map<std::string,Result>	results_;
    };
}

#endif
