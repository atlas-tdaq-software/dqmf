#ifndef _DQMF_INPUT_H_
#define _DQMF_INPUT_H_

/*! \file Input.h Declares the dqmf::Input class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/thread/tss.hpp>
#include <TObject.h>

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Framework.
 */
namespace dqmf
{
    /*! \brief This class provides a way of passing the required information 
    		from a DQM Input implementation to a DQM Output.
	\ingroup private
	\sa	dqm_core::Input
	\sa	dqm_core::Output
     */
    struct InputContext
    {        
        static void createThreadSpecificStorage();
        
        static void setData( int tag, TObject * data );        
        static int  getTag( );
        static TObject * getData( );
        
      private:
        InputContext();
        
	static boost::thread_specific_ptr<int> 		tag_;
	static boost::thread_specific_ptr<TObject *>	data_;
    };
}

#endif
