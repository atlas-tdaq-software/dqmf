#ifndef TAG_H
#define TAG_H

#include <is/info.h>

#include <string>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace dqmf
{

namespace is
{
/**
 * 
 * @author  generated by the IS tool
 * @version 13/02/07
 */

class Tag : public ISInfo {
public:

    /**
     */
    std::string                   name;

    /**
     */
    double                        value;


    static const ISType & type() {
	static const ISType type_ = Tag( ).ISInfo::type();
	return type_;
    }

    Tag( )
      : ISInfo( "Tag" )
    {
	initialize();
    }

    ~Tag(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Tag( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << value;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> value;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
}

}

#endif // TAG_H
