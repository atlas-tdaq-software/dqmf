#ifndef _DQMF_HISTOGRAM_RECEIVER_H_
#define _DQMF_HISTOGRAM_RECEIVER_H_

/*! \file HistogramReceiver.h Declares the dqmf::HistogramReceiver class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/noncopyable.hpp>

#include <dqm_core/InputListener.h>
#include <dqmf/Worker.h>

#include <is/inforeceiver.h>
#include <oh/OHRootReceiver.h>

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Framework.
 */
namespace dqmf
{
    /*! \brief This class implements the OH Root histogram receiver interface.
	\ingroup public
	\sa	dqmf::Input
     */
    class HistogramReceiver : 	public ISInfoReceiver,
    		   		public OHRootReceiver
    {
      public:
	
        HistogramReceiver(	const IPCPartition & partition, 
        			const std::string & name,
                                dqm_core::InputListener * listener,
                                dqmf::Worker & worker );

        virtual ~HistogramReceiver() = default;
    
        void addListener( dqm_core::InputListener * listener );

        void callbackReceived(const ISCallbackInfo * info );

      protected:
        virtual void notifyListeners( const char * name, int tag, TObject * object );
        
      protected:
	void receive( OHRootHistogram & ) override;
        void receive( OHRootGraph & ) override;
        void receive( OHRootGraph2D & ) override;
        void receive( OHRootEfficiency & eff ) override;

      protected:
        typedef std::vector<dqm_core::InputListener *> Listeners;
        
        Listeners	listeners_;
        dqmf::Worker &	worker_;
    };
}

#endif
