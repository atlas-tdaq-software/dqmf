#ifndef _DQMF_PHYSICS_INPUT_H_
#define _DQMF_PHYSICS_INPUT_H_

/*! \file PhysicsInput.h Declares the dqmf::PhysicsInput class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <is/inforeceiver.h>

#include <dqmf/Input.h>

/*! \namespace dqmf
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Framework.
 */
namespace dqmf
{
    /*! \brief This class provides extends the default implementation of the DQMF abstract input interface
    		so that it provides input only in the Physics mode.
	\ingroup public
	\sa	dqmf::Input
	\sa	dqm_core::Input
	\sa	dqm_core::InputListener
     */
    struct PhysicsInput : public dqmf::Input,
    			  public ISInfoReceiver
    {        
        /*! Constructor.
	 */
        PhysicsInput ( const std::vector<std::string> & parameters );
                
        void activate();

        void deactivate();
                        
      private:
	void callback( ISCallbackInfo * ci );
        
        void updateState( bool ready4phys );
      
      private:
	boost::mutex	m_mutex;
        bool		m_physics;
	bool		m_active;
    };
}

#endif
