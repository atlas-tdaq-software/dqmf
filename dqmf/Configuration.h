#ifndef _DQMF_CONFIGURATION_H_
#define _DQMF_CONFIGURATION_H_

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <ers/ers.h>

#include <dqm_core/Input.h>
#include <dqm_core/Output.h>
#include <dqm_core/Region.h>

#include <ipc/partition.h>

#include <config/Configuration.h>
#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQNode.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/AlgorithmConfigFactory.h>

#include <dal/util.h>

/*! \file Configuration.h Declares the dqmf::Configuration class.
 * \author Serguei Kolos
 * \version 1.0
 */

ERS_DECLARE_ISSUE(dqmf, BadState,
        "DQ Parameters can be disabled or enabled only in 'RUNNING' state", ERS_EMPTY)

namespace dqmf {
    class Configuration {
        typedef boost::shared_ptr<dqm_core::Region> RegionPtr;

        struct Helper {
            Helper() :
                    m_branch_disabled(false) {
                ;
            }

            RegionPtr m_current_parent;
            bool m_branch_disabled;
        };

    public:
        Configuration(::Configuration &config, const daq::core::Partition &partition,
                const std::string &agent_name);

        ~Configuration();

        void activate();

        void deactivate();

        void enableParameter(const std::string &name);

        void disableParameter(const std::string &name);

    private:
        void checkDisabled();

        void createIOStreams();

        bool isEnabled(const dqm_config::dal::DQNode *parameter);

        bool maybeChangeNodeState(const dqm_config::dal::DQNode *parameter,
                bool disable_children = false);

        void reset();

        bool referencedByAgent(const dqm_config::dal::DQRegion *region) const;

    private:
        bool build_region(const dqm_config::dal::DQRegion *region,
                dqmf::Configuration::Helper &parameter);

        bool update_status(const dqm_config::dal::DQRegion *region);

    private:
        ::Configuration &m_config;
        const daq::core::Partition &m_partition;
        dqm_config::AlgorithmConfigFactory m_algo_factory;
        std::string m_agent_name;
        std::unique_ptr<dqm_core::Input> m_input;
        std::unique_ptr<dqm_core::Output> m_output;
        const dqm_config::dal::DQAgent *m_agent;
        std::vector<RegionPtr> m_root_regions;
        uint32_t m_callback_handler;
        bool m_active;
    };
}

#endif
