# dqmf

The new DQMF configuration supports run type dependent values of DQ parameter thresholds and reference objects.

### Run type specific references

The **DQReference** class has a new attribute called *ValidFor*, which can be used to define a type of runs this reference object can be used. Available run types are:

* **PP** - proton-proton physics runs
* **HI** - heavy ion physics runs
* **Cosmic** - comics runs
* **Default** - any run type for which no reference is explicitly defined

One can associate multiple objects of **DQReference** class with the same DQ parameter, in which case DQMF will choose an appropriate one depending on the current run type. If no suitable **DQReference** object is defined for a valid run type, then **dqm_core::BadConfig** exception will be thrown and the corresponding DQ parameter will be ignored. It's therefore required to always have at least one instance of **DQReference** class with *Default* value of the **ValidFor** attribute associated with an DQ parameter that needs a reference. In addition one can also add specific references for PP, HI or Cosmic run types.

### Run type specific thresholds

The same run types can be used to customize values of **DQParameter** thresholds using a syntax described below. A common way of defining a threshold is to provide a string that has the following format:

```
<threshold> ::= <threshold name> = <threshold value>
<threshold name> ::= <string>
<threshold value> :: = <floating point number>
```

The new DQMF still supports this syntax and will use the given value as default. In addition one can also define different values for specific run types using more general syntax supported by the new version of DQM framework. 

```
<threshold value> ::= <run type specific value> [; <run type specific value>]
<run type specific value> ::= <run type> : <floating point number> | <floating point number>
<run type> ::= "pp" | "hi" | "cosmic" | "PP" | "HI" | "COSMIC"
```

More generally a threshold value may contain multiple tokens for different run types separated by semicolon. Each token either contains a run type and a floating point number separated by colon or just a floating point number, which in this case will be used as default value for all run types that don't have a specific value defined. For example:

```
MyThreshold=pp:0.123; hi:0.456; 0.789
```

This definition will tell DQMF to use the value *0.123* for *proton-proton* runs, the value *0.456* for *heavy ion* runs and the value *0.789* for any other run type. DQMF expects to get a well defined value of a threshold for all possible run types, which in practice means that a suitable default value must always be present in every threshold definition. If this is not the case DQMF will throw **dqm_core::BadConfig** exception and will ignore the corresponding DQ parameter. This approach is fully backward compatible as all existing threshold definitions will be treated as providing default values that are suitable for all run types.
