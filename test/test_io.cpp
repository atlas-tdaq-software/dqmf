/* test_core.cpp
   Author Serguei Kolos 15/09/06
*/

#include <boost/program_options.hpp>

#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/Region.h>
#include <dqm_core/IOManager.h>
#include <dqm_core/LibraryManager.h>
#include <dqm_core/test/DummyAlgorithmConfig.h>
#include <dqm_config/AlgorithmConfigFactory.h>

#include <ipc/core.h>
#include <ipc/partition.h>

int main(int argc, char **  argv) 
{
    IPCCore::init( argc, argv );
    
    boost::program_options::options_description description( "Options" );
    
    description.add_options()
	( "help", "produce help message" )
        ( "partition,p", boost::program_options::value<std::string>(), "TDAQ partition" )
	( "histograms,h", boost::program_options::value<std::vector<std::string> >()->multitoken(), "names of DQM histograms" )
    ;

    boost::program_options::variables_map arguments;
    try {
	boost::program_options::store( boost::program_options::parse_command_line( argc, argv, description ), arguments );
	boost::program_options::notify( arguments );
    }
    catch ( boost::program_options::error & ex )
    {
    	std::cerr << ex.what() << std::endl;
        description.print( std::cout );
        return 1;
    }
    
    if ( arguments.count("help") ) {
        std::cout << "Test application of the 'dqm' package" << std::endl;
        description.print( std::cout );
	return 0;
    }

    if ( !arguments.count("histograms") ) {
    	std::cerr << "'histograms' option is not given" << std::endl;
        description.print( std::cout );
        return 2;
    }
        
    std::vector<std::string> histograms	= arguments["histograms"].as<std::vector<std::string> >();
    
    IPCPartition partition;
    if ( arguments.count("partition") ) {
	partition = IPCPartition( arguments["partition"].as<std::string>() );
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // Uuu-f ... at last we have finished with parsing the command line
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    std::unique_ptr<dqm_core::Input>	input;
    std::unique_ptr<dqm_core::Output>	output;
    try
    {
	dqm_core::LibraryManager::instance().loadLibrary( "libdqm_algorithms.so" );
	dqm_core::LibraryManager::instance().loadLibrary( "libdqm_summaries.so" );
	dqm_core::LibraryManager::instance().loadLibrary( "libdqmf_io.so" );
        
        std::vector<std::string> params;
        params.push_back( partition.name() );
        input.reset( dqm_core::IOManager::instance().createInput( "DefaultInput", params ) );
        
	params.push_back( "DQM" );
	params.push_back( "DQMFTest" );
        output.reset( dqm_core::IOManager::instance().createOutput( "DefaultOutput", params ) );
    }
    catch (dqm_core::Exception & ex)
    {
	ers::fatal(ex);
	return -1;
    }

    dqm_core::RegionConfig rconfig( "SimpleSummary", 0.1 );
    boost::shared_ptr<dqm_core::Region> root;
    
    try
    {
    	root = dqm_core::Region::createRootRegion( "root", *input.get(), *output.get(), rconfig );
    }
    catch (dqm_core::Exception & ex)
    {
	ers::fatal(ex);
	return -1;
    }
      
    for ( size_t i = 0; i < histograms.size(); i++ )
    {
	try
	{
	    dqm_core::ParameterConfig pconfig( histograms[i], "Simple_gaus_Fit", 0.2,
		std::shared_ptr<dqm_core::AlgorithmConfig>(new dqm_core::test::DummyAlgorithmConfig) );
	    root->addParameter( histograms[i], pconfig );
        }
        catch( dqm_core::Exception & ex )
	{
	    ers::error( ex );
	}
    }
    
    output -> activate();
    input -> activate();
    input -> wait();    
}

