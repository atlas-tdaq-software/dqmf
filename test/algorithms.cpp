/*! \file algorithms.cpp implements DQ test algorithm and summary maker  
 * \author Serguei Kolos
*/

#include <TObjArray.h>
#include <TH1.h>
#include <TMap.h>
#include <TParameter.h>
 
#include <dqm_core/AlgorithmRegistrator.h>

#include <dqm_core/Algorithm.h>
#include <dqm_core/SummaryMaker.h>
#include <dqm_core/Parameter.h>

namespace
{
    struct TestAlgorithm : public dqm_core::Algorithm
    {
	dqm_core::Algorithm * clone( ) { return new TestAlgorithm( *this ); }


	dqm_core::Result * execute( const std::string & , const TObject & , const dqm_core::AlgorithmConfig & );

	void printDescriptionTo(std::ostream & out);
      
      protected:
        void fillResult( dqm_core::Result & result, const dqm_core::AlgorithmConfig & config );
    };

    struct TestISAlgorithm : public TestAlgorithm
    {
	dqm_core::Algorithm * clone( ) { return new TestISAlgorithm( *this ); }

	dqm_core::Result * execute( const std::string & , const TObject & , const dqm_core::AlgorithmConfig & );

	void  printDescriptionTo(std::ostream & out);
    };

    struct TestSummary : public dqm_core::SummaryMaker
    {
	SummaryMaker * clone() { return new TestSummary( *this ); }

	dqm_core::Result * execute( const std::string & ,
				    const dqm_core::Result & ,
				    const dqm_core::ParametersMap & );
    };
    
    dqm_core::AlgorithmRegistrator<TestSummary> 	__sm__( "TestSummary" );
    dqm_core::AlgorithmRegistrator<TestAlgorithm> 	__aa__( "TestAlgorithm" );
    dqm_core::AlgorithmRegistrator<TestISAlgorithm> 	__ii__( "TestISAlgorithm" );
}


void
TestAlgorithm::fillResult( dqm_core::Result & result, const dqm_core::AlgorithmConfig & config )
{
    result.tags_.insert(config.getParameters().begin(), config.getParameters().end());
    result.tags_.insert(config.getRedThresholds().begin(), config.getRedThresholds().end());
    result.tags_.insert(config.getGreenThresholds().begin(), config.getGreenThresholds().end());
}

dqm_core::Result *
TestAlgorithm::execute(	const std::string & name, 
			const TObject & ,
			const dqm_core::AlgorithmConfig & config )
{
    ERS_DEBUG( 1, "TestAlgorithm Executed for the '" << name << "' parameter" );

    dqm_core::Result * result = new dqm_core::Result( dqm_core::Result::Status( ::rand() % 4 ) );
    
    fillResult( *result, config );
    
    try {
        TH1 * ref = static_cast<TH1*>(config.getReference());
        result->tags_.insert(std::make_pair(ref->GetName(), 1.0));
    } catch (dqm_core::BadConfig & ex) {
        result->tags_.insert(std::make_pair("Reference is not defined", 0.0));
    }

    TObjArray * a = new TObjArray();
    a->SetOwner( true );
    for ( int i = 0; i < 5; ++i )
    {
	std::ostringstream o;
	o << "histogram" << i;
	std::string n = o.str();
	TH1F * h = new TH1F( n.c_str(), "Simple Gaussian Histogram", 100, -2, 2 );
	h->SetDirectory(0);
	h->FillRandom( "gaus", 1000 );
	a->Add( h );
    }
    
    result -> object_.reset( a );
    return result;
}

void
TestAlgorithm::printDescriptionTo(std::ostream & out)
{
    out << "TestAlgorithm: Test Algorithm to to be used for testing DQM Framework" << std::endl;

    out << "This algorithm will add its input parameters and thresholds to its result "
	    "as well as 5 random histograms." << std::endl;
}

dqm_core::Result *
TestISAlgorithm::execute( const std::string & name,
			  const TObject & object,
			  const dqm_core::AlgorithmConfig & config )
{
    ERS_DEBUG( 1, "TestISAlgorithm Executed for the '" << name << "' parameter" );

    dqm_core::Result * result = new dqm_core::Result( dqm_core::Result::Status( ::rand() % 4 ) );

    fillResult( *result, config );

    TMap * map = (TMap*)&object;
    TMapIter it(map);
    TObject * key= 0;
    while ( (key = it.Next()) != 0 )
    {
	TParameter<double>* param = (TParameter<double>*)map->GetValue(key);
	result->tags_.insert(std::make_pair(param->GetName(), param->GetVal()));
    }

    return result;
}

void
TestISAlgorithm::printDescriptionTo(std::ostream & out)
{
    out << "TestAlgorithm: Test Algorithm to to be used for testing DQM Framework" << std::endl;

    out << "This algorithm can check arbitrary IS information and put all input fields to its result." << std::endl;
}

dqm_core::Result * 
TestSummary::execute(	const std::string & name, 
			const dqm_core::Result & r,
			const dqm_core::ParametersMap & children )
{
    
    ERS_DEBUG( 1, "TestSummary executed for the '" << 
     	name << "' region and produces the following result: " << r );

    dqm_core::Result * result = new dqm_core::Result( r.status_ );
    
    dqm_core::ParametersMap::const_iterator it = children.begin();
    
    int counters[5] = { 0, 0, 0, 0, 0 };
    for ( ; it != children.end(); ++it )
    {
    	++counters[it->second->getResult()->status_ + 1];
    }
    
    result -> tags_["disabled"]	 = counters[0];
    result -> tags_["undefined"] = counters[1];
    result -> tags_["red"]	 = counters[2];
    result -> tags_["yellow"]	 = counters[3];
    result -> tags_["green"]	 = counters[4];
    
    return result;
}
